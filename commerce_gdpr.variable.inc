<?php

/**
 * Implements hook_variable_group_info().
 */
function commerce_gdpr_variable_group_info() {
  $groups['commerce_gdpr'] = array(
    'title' => t('Commerce GDPR'),
    'description' => t('Commerce GDPR Settings'),
    'access' => 'administer commerce gdpr',
  );
  return $groups;
}

/**
 * Implements hook_variable_info().
 */
function commerce_gdpr_variable_info($options) {
  $variables['commerce_gdpr_user_button_text'] = array(
    'type' => 'string',
    'title' => t('User account anonymization button text', array(), $options),
    'default' => TRUE,
    'description' => t('Defines the text of the button used to trigger the anonymization', array(), $options),
    'localize' => TRUE,
    'group' => 'commerce_gdpr',
  );

  return $variables;
}
